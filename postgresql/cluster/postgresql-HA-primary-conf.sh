#!/bin/bash
echo
echo "### Configuration of primary-server for High Availability ###"
echo

# deactivates monitoring of postgresql instance for configuring the cluster
#monit unmonitor postgres
#monit summary

echo "Waiting for the postgres server to start for 10 seconds..."

sleep 10

# creates user for replication
sudo -u postgres bash -c "psql -c \"CREATE USER replicator WITH PASSWORD '${POSTGRES_PASSWORD}' REPLICATION CONNECTION LIMIT 5;\""

#editing pg_hba.conf for allowing replication connections for HA from the standby instance
echo "Adding Standby IP to ${PGDATA}/pg_hba.conf"
echo -e "# Allow replication connections
host     replication     replicator         ${STANDBY_IP}/32        trust" >> ${PGDATA}/pg_hba.conf

#editing postgresql.conf for HA-configuration
echo "Changing values in /etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#listen_addresses =.*$|listen_addresses = '*'                    # what IP address(es) to listen on;|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#wal_level =.*$|wal_level = hot_standby                    # minimal, archive, or hot_standby|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#max_wal_senders =.*$|max_wal_senders = 3             # max number of walsender processes|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#checkpoint_segments =.*$|checkpoint_segments = 8                    # in logfile segments, min 1, 16MB each|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"
sed -i "s|^#wal_keep_segments  =.*$|wal_keep_segments  = 8                    # in logfile segments, 16MB each; 0 disables|" "/etc/postgresql/$PG_VERSION/main/postgresql.conf"


echo "Startging PostgreSQL"
sudo /bin/systemctl restart postgresql.service

# adding file for the set of scripts for checking if cluster is configured
mkdir -p $CHECK_PATH/
chmod 700 -R  $CHECK_PATH/
echo "If this file exists, initialy a cluster was configured." > $CHECK_PATH/primary-server

# activates monitoring of postgresql instance
#monit monitor postgres

#sleep 5

#monit summary
