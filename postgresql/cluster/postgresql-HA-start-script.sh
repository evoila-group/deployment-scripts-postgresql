#!/bin/bash

# This script only works with pre-installed and configured postgresql

export REPOSITORY_POSTGRESQL_HA="https://bitbucket.org/meshstack/deployment-scripts-postgresql/raw/HEAD/postgresql/cluster"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the PosgresQL-HA-Configuration
export PRIMARY_IP="192.168.11.44"
export STANDBY_IP="192.168.11.43"

wget $REPOSITORY_POSTGRESQL_HA/postgresql-HA-template.sh --no-cache
chmod +x postgresql-HA-template.sh
./postgresql-HA-template.sh -u evoila -p evoila -d evoila -e openstack -i ${PRIMARY_IP} -j ${STANDBY_IP} -a primary



###################### Secondary Configuration ######################

#!/bin/bash
export REPOSITORY_POSTGRESQL_HA="https://bitbucket.org/meshstack/deployment-scripts-postgresql/raw/HEAD/postgresql/cluster"

# Put in here the IP-Adresses for the Primary-Instance and the Standby-Instance of the PosgresQL-HA-Configuration
export PRIMARY_IP="192.168.11.40"
export STANDBY_IP="192.168.11.41"

wget $REPOSITORY_POSTGRESQL_HA/postgresql-HA-template.sh --no-cache
chmod +x postgresql-HA-template.sh
./postgresql-HA-template.sh -u servicedb -p evoila -d servicedb -e openstack -i ${PRIMARY_IP} -j ${STANDBY_IP} -a standby
