#!/bin/bash

export PG_VERSION=9.5
export PGPATH=/usr/lib/postgresql/$PG_VERSION/bin
export PATH=$PGPATH:$PATH
export PGDATA=/data/postgres
export CHECK_PATH=/data/postgres-cluster-check

export OPENSTACK_START="/bin/systemctl restart postgresql.service"
export OPENSTACK_STOP="/bin/systemctl stop postgresql.service"


#parameters for dbname, user and password used in following scripts
usage() { echo "Usage: $0 [-d <string>] [-u <string>] [-p <string>] [-e <string>] [-a <string>] [-i <string>] [-j <string>]" 1>&2; exit 1; }

while getopts ":d:u:p:e:a:i:j:" o; do
    case "${o}" in
        d)
            POSTGRES_DB=${OPTARG}
            ;;
        p)
            POSTGRES_PASSWORD=${OPTARG}
            ;;
        u)
            POSTGRES_USER=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        a)
            HA_INSTANCE_TYPE=${OPTARG}
            ;;
        i)
            PRIMARY_IP=${OPTARG}
            ;;
        j)
            STANDBY_IP=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${POSTGRES_DB}" ] || [ -z "${POSTGRES_USER}" ] || [ -z "${POSTGRES_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ] || [ -z "${HA_INSTANCE_TYPE}" ] || [ -z "${PRIMARY_IP}" ] || [ -z "${STANDBY_IP}" ]; then
    usage
fi

#exports from parameters for following scripts
export POSTGRES_DB=${POSTGRES_DB}
export POSTGRES_USER=${POSTGRES_USER}
export POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}
export HA_INSTANCE_TYPE=${HA_INSTANCE_TYPE}
export PRIMARY_IP=${PRIMARY_IP}
export STANDBY_IP=${STANDBY_IP}

echo "postgres_db = ${POSTGRES_DB}"
echo "postgres_user = ${POSTGRES_USER}"
echo "environment = ${ENVIRONMENT}"
echo "repository_postgresql_ha = ${REPOSITORY_POSTGRESQL_HA}"
echo "check_path = ${CHECK_PATH}"
echo "ha_instance_type = ${HA_INSTANCE_TYPE}"
echo "primary_ip = ${PRIMARY_IP}"
echo "standby_ip = ${STANDBY_IP}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of postgresql
  $OPENSTACK_START
else
  if [ "$HA_INSTANCE_TYPE" = 'primary' ]; then
    # loads and executes script for configuration of the primary-postgresql-instance for HA
    wget $REPOSITORY_POSTGRESQL_HA/postgresql-HA-primary-conf.sh
    chmod +x postgresql-HA-primary-conf.sh
    ./postgresql-HA-primary-conf.sh
  fi

  if [ "$HA_INSTANCE_TYPE" = 'standby' ]; then
    # loads and executes script for configuration of the standby-postgresql-instance for HA
    wget $REPOSITORY_POSTGRESQL_HA/postgresql-HA-standby-conf.sh
    chmod +x postgresql-HA-standby-conf.sh
    ./postgresql-HA-standby-conf.sh
  fi
fi
