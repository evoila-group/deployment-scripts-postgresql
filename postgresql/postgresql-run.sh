#!/bin/bash
echo
echo "### Starting postgres-run.sh ... ###"
echo

# starts postgresql
	# starts postgresql for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
		$OPENSTACK_START
		echo "started with openstack"
fi

echo "  ┌────────────────────────────────────────────────────────────────┬─────────────────┐"
echo "  │   ┬ ┬ ┬ ┬ ┬ ┬    ╔══╗  ╦   ╦  ╔══╗  ╦  ╦    ╔══╗     ┌┬┐ ┌─┐   │                 │"
echo "  │   │││ │││ │││    ║═╣   ╚╗ ╔╝  ║  ║  ║  ║    ╠══╣      ││ ├┤    │   evoila GmbH   │"
echo "  │   └┴┘ └┴┘ └┴┘ o  ╚══╝   ╚═╝   ╚══╝  ╩  ╩══╝ ╩  ╩  o  ─┴┘ └─┘   │ Mainz / Germany │"
echo "  │                                                                │                 │"
echo "  │    ┌─┐┌─┐┬─┐┬  ┬┬┌─┐┌─┐  ┬┌─┐  ┌─┐┌┬┐┌─┐┬─┐┌┬┐┬┌┐┌┌─┐          │  www.evoila.de  │"
echo "  │    └─┐├┤ ├┬┘└┐┌┘││  ├┤   │└─┐  └─┐ │ ├─┤├┬┘ │ │││││ ┬          │ info@evoila.de  │"
echo "  │    └─┘└─┘┴└─ └┘ ┴└─┘└─┘  ┴└─┘  └─┘ ┴ ┴ ┴┴└─ ┴ ┴┘└┘└─┘  o o o   │                 │"
echo "  └────────────────────────────────────────────────────────────────┴─────────────────┘"

# starts postgresql
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 sudo -u postgres $DOCKER_START
 echo "started with docker"
fi

#monit summary
