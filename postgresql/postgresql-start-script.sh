#!/bin/bash

export REPOSITORY_POSTGRESQL="https://bitbucket.org/meshstack/deployment-scripts-postgresql/raw/HEAD/postgresql"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_POSTGRESQL/postgresql-template.sh --no-cache
chmod +x postgresql-template.sh
./postgresql-template.sh -u evoila -p evoila -d evoila -e openstack
