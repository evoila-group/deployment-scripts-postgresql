#!/bin/bash

export PG_VERSION=9.5
export PGPATH=/usr/lib/postgresql/$PG_VERSION/bin
export PATH=$PGPATH:$PATH
export PGDATA=/data/postgres


#path used to check if service is installed
export CHECK_PATH=/data/postgres

export OPENSTACK_START="/bin/systemctl start postgresql.service"
export OPENSTACK_STOP="/bin/systemctl stop postgresql.service"
export DOCKER_START="${PGPATH}/pg_ctl start -D ${PGDATA} -o '--config-file=/etc/postgresql/$PG_VERSION/main/postgresql.conf'"
export DOCKER_STOP="${PGPATH}/pg_ctl -w stop"

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"


#parameters for dbname, user and password used in following scripts
usage() { echo "Usage: $0 [-d <string>] [-u <string>] [-p <string>] [-e <string>]" 1>&2; exit 1; }

while getopts ":d:u:p:e:" o; do
    case "${o}" in
        d)
            POSTGRES_DB=${OPTARG}
            ;;
        p)
            POSTGRES_PASSWORD=${OPTARG}
            ;;
        u)
            POSTGRES_USER=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${POSTGRES_DB}" ] || [ -z "${POSTGRES_USER}" ] || [ -z "${POSTGRES_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi

export POSTGRES_DB=${POSTGRES_DB}
export POSTGRES_USER=${POSTGRES_USER}
export POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}

echo "postgres_db = ${POSTGRES_DB}"
echo "postgres_user = ${POSTGRES_USER}"
echo "environment = ${ENVIRONMENT}"
echo "repository_postgresql = ${REPOSITORY_POSTGRESQL}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of postgresql
    chmod +x postgres-run.sh
    ./postgres-run.sh postgres
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_POSTGRESQL/postgresql-logging.sh
      chmod +x postgresql-logging.sh
      ./postgresql-install.sh
    fi
    # loads and executes script for automatic installation of postgresql
    wget $REPOSITORY_POSTGRESQL/postgresql-install.sh
    chmod +x postgresql-install.sh
    ./postgresql-install.sh

    # loads includes for the monit controlfile for this database
    #mkdir -p /etc/monit.d/
    #wget $REPOSITORY_POSTGRESQL/postgresql-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of postgresql
    wget $REPOSITORY_POSTGRESQL/postgresql-configuration.sh
    chmod +x postgresql-configuration.sh
    ./postgresql-configuration.sh

    # loads and executes script for startup of postgresql
    wget $REPOSITORY_POSTGRESQL/postgresql-run.sh
    chmod +x postgresql-run.sh
    ./postgresql-run.sh postgres
fi

# installs monit for openstack as environment
#if [ "$ENVIRONMENT" = 'openstack' ]; then
#  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
#  wget $REPOSITORY_MONIT/monit-template.sh
#  chmod +x monit-template.sh
#  ./monit-template.sh -u monit -p ${POSTGRES_PASSWORD}
#fi
